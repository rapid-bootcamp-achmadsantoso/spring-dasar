package com.rapidtech.springdasar.config.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProductCategory {
    private Product product;
    private Category category;
}
