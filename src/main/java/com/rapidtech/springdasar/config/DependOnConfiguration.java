package com.rapidtech.springdasar.config;

import com.rapidtech.springdasar.config.model.Bar;
import com.rapidtech.springdasar.config.model.Foo;
import com.rapidtech.springdasar.doubletonScope.DoubletonScope;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.context.annotation.*;

@Slf4j
@Configuration
public class DependOnConfiguration {

    @Bean
    @Scope("prototype")
    public Foo foo() {
        log.debug("Creating new Foo");
        return new Foo();
    }

    @Bean
    public CustomScopeConfigurer customScopeConfigurer(){
        CustomScopeConfigurer configurer = new CustomScopeConfigurer();
        configurer.addScope("doubleton", new DoubletonScope());
        return configurer;
    }
//    @Lazy
    @Bean
    @Scope("doubleton")
    @DependsOn(value = {"foo"})
    public Bar bar() {
        log.debug("Creating new Bar");
        return new Bar();
    }
}

