package com.rapidtech.springdasar.singleton;

public class Person {
    private static Person person;

    public static Person getInstance(){
        if(person==null){
            person = new Person();
        }
        return person;
    }

    private Person(){

    }
}
