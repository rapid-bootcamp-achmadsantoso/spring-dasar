package com.rapidtech.springdasar.config;

import com.rapidtech.springdasar.config.model.Bar;
import com.rapidtech.springdasar.config.model.Foo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

class DependOnConfigurationTest {
    ApplicationContext context = new AnnotationConfigApplicationContext(DependOnConfiguration.class);

    @Test
    void dependOnTest() {
        Foo foo1 = context.getBean(Foo.class);
        Foo foo2 = context.getBean(Foo.class);
        Foo foo3 = context.getBean(Foo.class);

        //FOO PROTOTYPE, JADI TIDAK SAMA OBJECTNYA
        Assertions.assertNotSame(foo1, foo2);
        Assertions.assertNotSame(foo1, foo3);


        //Bar bar1 = context.getBean(Bar.class);
        //Bar bar2 = context.getBean(Bar.class);
        //Assertions.assertSame(bar1, bar2);
    }

    @Test
    void TestDoubletonScope(){
        Bar bar1 = context.getBean(Bar.class);
        Bar bar2 = context.getBean(Bar.class);
        Bar bar3 = context.getBean(Bar.class);
        Bar bar4 = context.getBean(Bar.class);

        Assertions.assertSame(bar1, bar3);
        Assertions.assertSame(bar2, bar4);

        Assertions.assertNotSame(bar1, bar2);
        Assertions.assertNotSame(bar3, bar4);

    }

}